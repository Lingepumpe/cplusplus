#include <iostream>

#include "graph.h"

int main() {
  srand (time(NULL));
  Graph g{5};
  std::cout << "g{5}: " + g.toString() + "\n";
  Graph h(5, 5.5);
  std::cout << "h{5, 5.5}: " + h.toString() + "\n";

  h.search(0, 3);

  Graph a{5};
  const Graph b{3};
  std::cout << "a: " << a.toString() + "\n";
  std::cout << "b: " << b.toString() + "\n";
}
