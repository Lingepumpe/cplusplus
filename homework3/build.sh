#!/bin/bash
set -e
set -x

CPP="g++"

CPPFLAGS="-std=c++11 -Wall -ggdb"

${CPP} ${CPPFLAGS} main.cpp graph.cpp -o my_graph

