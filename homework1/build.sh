#!/bin/bash
set -e
set -x

CC="gcc"
CPP="g++"

CPPFLAGS="-Wall -ggdb"
CFLAGS="${CFLAGS}"

${CPP} ${CPPFLAGS} -c hw2_cpp.cpp
${CC} -c hw2_c.c 
${CPP} hw2_cpp.o hw2_c.o -o hw2

