#include <iostream>
#include "hw2_cpp.h"
#include "hw2_c.h"

#define PRINT_CALL(s) { std::cout << #s " -> " << s << std::endl; }


/*
 * We can see here C++ code first calling a function from C code (c_add)
 * this is accomplished by making sure the function is declared as 
 * "extern "C".
 * Then we see our code calling annother C function, which in turn calls
 * back into the C++ code, effectively demonstrationg the reverse way,
 * C code calling C++ code. We accomplish this by providing a wrapper
 * interface which is C only in our hw2_cpp.h header file - this way our
 * wrapper function can be called from C, then it is passed on to our "real"
 * c++ function to do the actual work.
 * Limitations: We see it is not possible to use overloading in the interface
 * we wish to expose to the C code: cpp_add_wrapper can only add ints, to add
 * doubles we needed a new name (cpp_add_double_wrapper).
 * Further limitations (not shown in code): Any C++ specific types, e.g. std::string,
 * std::map or std::vector do not exist in C, hence cannot be expected to be
 * passed in or received out of the wrapper functions. Conversion is possible though,
 * for example take a "const char*" parameter in the wrapper interface, construct
 * a std::string from it in the wrapper function implementation, then call the
 * worker cpp function with the std::string parameter.
 */

int main() {
  std::cout << "C++ code calling C code" << std::endl;
  PRINT_CALL(c_add(2, 3));
  std::cout << "C++ code calling C code calling C++ code" << std::endl;
  PRINT_CALL(c_call_cpp_add(2, 3));
  return 0;
}

int cpp_add(int a, int b) {
  return a + b;
}

double cpp_add(double a, double b) {
  return a + b;
} 

int cpp_add_wrapper(int a, int b) {
  return cpp_add(a, b);
}

double cpp_add_double_wrapper(double a, double b) {
  return cpp_add(a, b);
}

