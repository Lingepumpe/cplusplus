#ifndef HW2_CPP_H
#define HW2_CPP_H

#ifdef __cplusplus
extern "C" {
#endif

int cpp_add_wrapper(int, int);
double cpp_add_double_wrapper(double, double);

#ifdef __cplusplus
}
#endif

#endif  // HW2_CPP_H

