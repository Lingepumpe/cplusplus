#include "hw2_c.h"
#include "hw2_cpp.h"

int c_add(int a, int b) {
  return a+b;
}

int c_call_cpp_add(int a, int b) {
  return cpp_add_wrapper(a, b);
}

int c_call_cpp_add_double(double a, double b) {
  return cpp_add_double_wrapper(a, b);
}

