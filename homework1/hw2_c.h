#ifndef HW2_C_H
#define HW2_C_H

#ifdef __cplusplus
extern "C" {
#endif

int c_add(int, int);
int c_call_cpp_add(int, int);

#ifdef __cplusplus
}
#endif

#endif  // HW2_C_H

