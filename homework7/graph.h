#ifndef GRAPH_H
#define GRAPH_H

#include <list>
#include <vector>
#include <string>

using namespace std;

/* A undirected graph of vertices named 0... V-1
 *
 * Primary operations:
 * Add edge between two vertices
 * Add vertex to the graph
 *
 * Design:
 * Adjacency list for for each vertex. We allow parallel edges and self-loops.
 * By convention, a self-loop v-v appears in the
 * adjacency list of v twice and contributes two to the degree
 * of v.
 */

class Graph {
public:
    typedef std::vector<int>::size_type size_type;

    Graph(); //construct empty graph
    explicit Graph(size_type V); //create with V vertices and no edges

    Graph(const Graph& src); //copy constructor
    Graph(Graph&& src); //move constructor

    virtual ~Graph() {} //destructor

    Graph& operator=(Graph other); //copy assign constructor
    friend void swap(Graph& first, Graph& second); //nothrow

    virtual Graph& addVertex();
    virtual Graph& addEdge(size_type v, size_type w);

    virtual Graph& addRandomEdges(size_type n); //adds n random edges
    virtual Graph& makeConnected(); //adds edges that make the graph connected

    virtual const list<size_type>& getAdj(size_type v) const;
    virtual bool existNode(size_type node) const { return node < V; }
    virtual int getV() const { return V; }
    virtual int getE() const { return E; }

    virtual int minPath(size_type v, size_type w) const; //length of minimum path from v to w, or -1 if no path exists
    struct ShortestPathInfo {
    ShortestPathInfo(size_type n, size_type source) : distTo(n, -1), from(n, 0), source(source) {}
        string toString() const;
        vector<int> distTo;
        vector<size_type> from;
        size_type source;
    };
    virtual ShortestPathInfo dijkstraSP(int source) const;


    virtual std::string toString() const;

 protected:
    virtual int getWeight(size_type v, size_type w) const; //returns 1 if edge exists, -1 otherwise (since graph is unweighted)

    size_type V; //number vertices
    size_type E = 0; //number of edges
    vector<list<size_type> > adj;
};

#endif //GRAPH_H
