#!/bin/bash
set -e
set -x

CPP="g++"

CPPFLAGS="-std=c++11 -Wall -ggdb"

${CPP} ${CPPFLAGS} graph.cpp weighted_graph.cpp main_graph.cpp -o graph
