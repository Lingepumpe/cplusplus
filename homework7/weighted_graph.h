#ifndef WEIGHTED_GRAPH_H
#define WEIGHTED_GRAPH_H

#include "graph.h"
#include "assert.h"
#include <sstream>
#include <iostream>

using namespace std;

class WeightedGraph : public Graph {
public:
    WeightedGraph() = default;
    WeightedGraph(size_type V);
    virtual ~WeightedGraph() {}

    virtual WeightedGraph& addVertex() override;
    virtual WeightedGraph& addEdge(size_type v, size_type w) override;
    virtual WeightedGraph& addEdge(size_type v, size_type w, unsigned int weight);
    virtual int getWeight(size_type v, size_type w) const override; //returns the smallest weight of an edge between v and w

    virtual WeightedGraph& addRandomEdges(size_type n) override; //adds n random edges with weights between 0 and 10

    virtual std::string toString() const override;
    
    template<class Pred> void print_edge_if(Pred p) {
        std::stringstream ss;
        ss << "Predicated edges\n";
        for(size_type v = 0; v < V; ++v) {
            auto weightIter = weight[v].begin();
            for(auto adjIter = adj[v].begin(); adjIter != adj[v].end(); ++adjIter, ++weightIter) {
                size_type w = *adjIter;
                if(w > v) { //prevent double printing edges
                    if(p(w, v, *weightIter)) {
                        ss << v << "<->" << w << " (weight=" << *weightIter << "), ";
                    }
                } else if(w == v) { //loop
                    if(p(w, v, *weightIter)) {
                        ss << v << "<->" << v << " (weight=" << *weightIter << "), ";
                    }
                    ++adjIter; //dont print double for loops either
                    ++weightIter;
                }
            }
        }
        cout << ss.str() << "\n";
    }

protected:
    vector<list<unsigned int>> weight;
};


#endif // WEIGHTED_GRAPH_H
