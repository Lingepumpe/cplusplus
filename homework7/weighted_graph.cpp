#include <limits>
#include <sstream>
#include <iostream>

#include "weighted_graph.h"

typedef Graph::size_type size_type;

WeightedGraph::WeightedGraph(size_type V) : Graph{V}, weight{V} {
}

WeightedGraph& WeightedGraph::addVertex() {
    Graph::addVertex();
    weight.resize(V);
    return *this;
}

WeightedGraph& WeightedGraph::addEdge(size_type v, size_type w) {
    return addEdge(v, w, 1);
}

WeightedGraph& WeightedGraph::addEdge(size_type v, size_type w, unsigned int edgeWeight) {
    Graph::addEdge(v, w);
    weight[v].push_back(edgeWeight);
    weight[w].push_back(edgeWeight);
    return *this;
}

WeightedGraph& WeightedGraph::addRandomEdges(size_type n) {
    for(size_type i = 0; i < n; ++i) {
        addEdge(rand() % V, rand() % V, rand() % 10);
    }
    return *this;
}

int WeightedGraph::getWeight(size_type v, size_type w) const {
    int min = -1;
    auto adjIter = adj[v].begin();
    auto weightIter = weight[v].begin();
    for(; weightIter != weight[v].end(); ++adjIter, ++weightIter) {
        if(*adjIter == w) {
            if((int)*weightIter < min || min == -1) {
                min = (int)*weightIter;
            }
        }
    }
    return min;
}

std::string WeightedGraph::toString() const {
    std::stringstream ss;
    ss << "Weighted Graph with V=" << V << ", E=" << E << "\nEdges: ";
    for(size_type v = 0; v < V; ++v) {
        auto weightIter = weight[v].begin();
        for(auto adjIter = adj[v].begin(); adjIter != adj[v].end(); ++adjIter, ++weightIter) {
            size_type w = *adjIter;
            if(w > v) { //prevent double printing edges
                ss << v << "<->" << w << " (weight=" << *weightIter << "), ";
            } else if(w == v) { //loop
                ss << v << "<->" << v << " (weight=" << *weightIter << "), ";
                ++adjIter; //dont print double for loops either
                ++weightIter;
            }
        }
    }
    ss << "\n";
    return ss.str();
}
