#include <iostream>
#include <memory>
#include <math.h>
#include <string.h>
#include <set>
#include <algorithm>

#include "graph.h"
#include "weighted_graph.h"

using namespace std;

unique_ptr<int[]> sievePrimes(long long n) {
    int* memory = new int[n];
    memset(memory, 0, sizeof(int)*n);
    unique_ptr<int[]> in(memory);
    for(long long i = 2; i < sqrt(n); ++i) {
        if(in[i] == 0) { //this is a prime
            for(long long j = i*i; j < n; j += i) {
                in[j] = 1;
            }
        }
    }
    return in;
}

int main() {
    srand(time(nullptr));
    
    Graph g{4};
    g.addRandomEdges(6);
    cout << g.toString() << "\n";
    cout << g.dijkstraSP(0).toString();

    cout << "\n======================\n\n";
    
    WeightedGraph wg{5};
    wg.addRandomEdges(7);
    cout << wg.toString() << "\n";
    cout << wg.dijkstraSP(3).toString();

    auto pred = [] (Graph::size_type v, Graph::size_type w, unsigned int weight) -> bool { return weight > 5; };
    cout << "weight > 5 ";
    wg.print_edge_if(pred);

    long long max_prime = -1;
    unique_ptr<int[]> is_not_prime;
    auto pred2 = [&] (Graph::size_type v, Graph::size_type w, unsigned int weight) -> bool { if(weight > max_prime) { is_not_prime = sievePrimes(weight+1); max_prime = weight; } return is_not_prime[weight] == 0; };
    cout << "weight is prime ";
    wg.print_edge_if(pred2);

    cout << "/******* Task 2 *******/\n";
    vector<int> vec = { 55, 1, 2, 5, 99, 55, 7, 13, 11, -5, 17, -13, 25, -1, -45, -22, 19 };
    int v = 23;
    set<int> completingNumbers;
    auto pred_adds_to_v = [&] (int val) { bool result = (completingNumbers.find(val) != completingNumbers.end()); completingNumbers.insert(v - val); return result; };
    auto pos = find_if(vec.begin(), vec.end(), pred_adds_to_v);
    cout << (pos == vec.end() ? "No two values add up to " + to_string(v) : "Two values add up to " + to_string(v) + ", and one of them is " + to_string(*pos)) << "\n";

    class AlternativePred {
    public:
        AlternativePred(int v) : m_v(v) {}
        bool operator()(int val) {
            bool result = (m_completingNumbers.find(val) != m_completingNumbers.end());
            m_completingNumbers.insert(m_v - val);
            return result;
        }
private:
        int m_v;
        set<int> m_completingNumbers;
    };

    v = 15;
    auto pos2 = find_if(vec.begin(), vec.end(), AlternativePred(v));
    cout << (pos2 == vec.end() ? "No two values add up to " + to_string(v) : "Two values add up to " + to_string(v) + ", and one of them is " + to_string(*pos2)) << "\n";
}
