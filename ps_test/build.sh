#!/bin/bash
set -e
set -x

CPP="g++"

CPPFLAGS="-std=c++11 -Wall -ggdb"

${CPP} ${CPPFLAGS} task1.cpp -o task1
${CPP} ${CPPFLAGS} task2.cpp -o task2
${CPP} ${CPPFLAGS} task3.cpp -o task3


