#include <iostream>

template<size_t A, size_t B>
struct mcd {
    static const size_t value = mcd<B, A%B>::value;
};

template<size_t A>
struct mcd<A, 0> {
    static const size_t value = A;
};

int main() {
	// I expect to be able to call your template as follows
	unsigned m = mcd<26u,2032u>::value;
	std::cout << m << std::endl;

	unsigned n = mcd<418u,73u>::value; //73 is prime
	std::cout << n << std::endl;

	unsigned o = mcd<96u,160u>::value; //96 = 3*32, 160 = 5*32, gcd = 32
	std::cout << o << std::endl;


}
