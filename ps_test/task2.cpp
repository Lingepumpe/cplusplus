#include<iostream>
#include<memory>
#include<vector>
#include<list>

struct List {
    ~List() {
	std::list<List*> to_del;
	for(List* cur = n.release(); cur != nullptr; cur = cur->n.release()) {
		to_del.push_back(cur);
	}
	for(auto e : to_del) {
		delete e;
	}
    }
	unsigned value;

	std::unique_ptr<List> n;
	
	List(unsigned v, std::unique_ptr<List> next) : value(v), n(std::move(next)) {}

	List(unsigned v) : value(v) {}

};


int main() {
	std::unique_ptr<List>  head(new List(1, std::unique_ptr<List>(new List(0))));
	
	for (int i = 2; i < 1000000; i++) {
		std::unique_ptr<List> head2(new List(i,std::move(head)));
		head = std::move(head2);	
	} 

	std::cout << "Printed !" << std::endl;

	return 0;
}


