#include <iostream>
#include <functional>
#include <set>
#include <string>
#include <algorithm>

using std::set;
using std::string;

int main() {
	set<string> text;
	text.insert("En");
	text.insert("un");
	text.insert("lugar");
	text.insert("de");
	text.insert("la");
	text.insert("mancha");
	text.insert("de");
	text.insert("cuyo");
	text.insert("nombre");
	text.insert("no");
	text.insert("quiero");
	text.insert("acordarme");	
	text.insert("vivia");
	text.insert("un");
	text.insert("hidalgo");
	text.insert("llamado");
	text.insert("don");
	text.insert("quijote");

	set<string> keys;
	keys.insert("vivia");
	keys.insert("acordarme");
	keys.insert("quijote");
	keys.insert("sancho");


	// Your code here.
	// Check all words within keys are included in text
	// Restrictions: you got to do it using std::find_if
    // and lambda functions. You can use an O(n^2) solution

    //Complexity of our solution:
    //n = text.size()
    //m = keys.size()
    //our find_if goes once over text => O(n)
    //our find_if also goes through keys, but bounded by n steps only, so we remain in O(n)
    //final verdict: O(n)

    auto cur = keys.begin();
    int number_found = keys.size();
    auto lambda = [&] (const std::string& elem) {
        if(cur == keys.end()) { //we have exhausted all our keys, we are done
            return true;
        }
        if(elem > *cur) { //sets are sorted, if elem is greater than cur, we will not reach cur anymore, we abort
            return true;
        }
        if(elem == *cur) {
            --number_found;
            ++cur;
        }
        return false;
    };

    find_if(text.begin(), text.end(), lambda);
    if(number_found == 0) {
        std::cout << "We found all keys!\n";
    } else {
        std::cout << "We did not find all keys\n";
    }

    /* Now try an example where we have all keys */
    text.insert("sancho");
    number_found = keys.size();
    cur = keys.begin();
    find_if(text.begin(), text.end(), lambda);
    if(number_found == 0) {
        std::cout << "We found all keys!\n";
    } else {
        std::cout << "We did not find all keys\n";
    }
};




	


