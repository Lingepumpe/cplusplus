# Advanced C/C++ programming

No group work.

## Contents

Homework solutions. One exercise per week. Presentation of the solution
of random others.

## Links

Course material: http://dps.uibk.ac.at/~juan/lectures.html
No group work.

# Scoring

Not clearly defined. Function of:

- Exercises submitted.
- Review of other students code
- Presentation of other students code
- final test

