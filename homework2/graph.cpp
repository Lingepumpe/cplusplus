#include <sstream>

#include "graph.h"

Graph::Graph(int V) : V{V}, E{0}, adj(V) {
}

void Graph::addEdge(int v, int w) {
  adj[v].push_back(w);
  adj[w].push_back(v);
  ++E;
}

const std::list<int>& Graph::getAdj(int v) const {
  return adj[v];
}

int Graph::getV() const {
  return V;
}

int Graph::getE() const {
  return E;
}

std::string Graph::toString() const {
  std::stringstream ss;
  ss << "Graph with V=" << V << ", E=" << E;
  return ss.str();
}
