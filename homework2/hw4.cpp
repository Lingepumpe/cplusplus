#include <iostream>

#include "graph.h"

int main() {
  Graph g{5};
  std::cout << g.toString() + "\n";
  g.addEdge(0, 4);
  g.addEdge(1, 4);
  g.addEdge(2, 4);
  g.addEdge(3, 4);
  std::cout << g.toString() << "\n";
  
  auto v = 4;
  for(auto w : g.getAdj(v)) {
    std::cout << v << " is connected to " << w << "\n";
  }

}
