#include <iostream>
#include <exception>
#include <stdexcept> //include required for std::runtime_error


int *a, *b, *c;
void allocate() {
   a = new int(1);
   b = new int(2);
   c = new int(3);
}
void deallocate() {
   std::cout << "Deallocating objects" << std::endl;
   delete a;
   delete b;
   delete c;
}
int main() {
  allocate();
  /*
  //Solution1: surround with try catch
  try { //added try catch to make sure deallocate is called if the contained code throws an exception (which it does)
    throw std::runtime_error(" :) ");
  } catch(...) {}
  deallocate();
  */

  //Solution2: overwrite terminate handler
  std::set_terminate (deallocate);
  throw std::runtime_error(" :) ");
  deallocate();
}
