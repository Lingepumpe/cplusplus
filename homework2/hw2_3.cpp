#include <iostream>
#include <fstream>

void openFile(std::fstream &file) {
  //problem here, open fails, we do not specify fstream::out flag, also trunc and append dont work well together
  //c++11 standard says: If the mode has both trunc and app set, the opening operation fails. It also fails if trunc is set but out is not.
  file.open("test.txt", std::fstream::trunc | std::fstream::app);
}

void writeToFile(std::fstream &file) {
  file << "hola" << std::endl;
}

void closeFile(std::fstream &file) {
  file.close();
}

int main() {
  std::fstream file;
  openFile(file); //this doesnt work :)
  file.close();
  file.open("test.txt", std::fstream::trunc | std::fstream::out);
  writeToFile(file);
  closeFile(file);
  std::cout << "Reached end\n";
}
