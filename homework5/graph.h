#ifndef GRAPH_H
#define GRAPH_H

#include <list>
#include <vector>

/* A undirected graph of vertices named 0... V-1
 * It supports the following two primary operations: add an edge to the graph,
 * add a vertex to the graph,
 * iterate over all of the vertices adjacent to a vertex. It also provides
 * methods for returning the number of vertices V and the number
 * of edges E. Parallel edges and self-loops are permitted.
 * By convention, a self-loop v-v appears in the
 * adjacency list of v twice and contributes two to the degree
 * of v.
 * 
 * This implementation uses an adjacency-lists representation, which 
 * is a vertex-indexed vector of std::vector of adacent vertices.
 */


class Graph {
 public:
  Graph();
  Graph(int V); //create with random edges
  Graph(int V, double ugly); //create fully connected

  Graph(const Graph& src); //copy constructor. To disallow the passing of a graph by value simply set this to " = delete;"...
  Graph(Graph&& src); //move constructor

  ~Graph() {}

  Graph& operator=(Graph other); //copy assign constructor

  friend void swap(Graph& first, Graph& second); //nothrow

  Graph& addVertex();
  Graph& addEdge(int v, int w);

  void search(int v, int w) const;

  const std::vector<int>& getAdj(int v) const;
  bool existNode(int node) const { return node >= 0 && node < V; }
  bool isConnected() const;
  int getV() const;
  int getE() const;
  std::string toString();
  std::string toString() const;

 private:
  std::string toStringHelper(bool isConst) const;

  int V; //number vertices
  int E = 0; //number of edges
  std::vector<std::vector<int> > adj;
};

#endif //GRAPH_H
