#ifndef MY_UNIQUE_PTR_H
#define MY_UNIQUE_PTR_H

#include <atomic>

class MyUniquePtr {
public:
    MyUniquePtr();
    MyUniquePtr(int x, int y);
    MyUniquePtr(const MyUniquePtr&) = delete; //you cannot copy a unique pointer
    MyUniquePtr(MyUniquePtr&& other);
    ~MyUniquePtr();

    MyUniquePtr& operator=(const MyUniquePtr&) = delete; //you cannot copy assign a unique pointer
    MyUniquePtr& operator=(MyUniquePtr&&); //you can move assign a unique pointer
    
    int** get() const;
    void reset();

private:
    void freeMemory();

    int** memory;
    int x;
};

#endif //MY_UNIQUE_PTR_H
