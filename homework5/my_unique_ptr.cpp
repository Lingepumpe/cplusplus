#include <algorithm> //for std::swap
#include <iostream>
#include <iomanip> //for std::setw
#include <vector>

#include "my_unique_ptr.h"

using namespace std;

#define TRACE(s) cout << __FILE__ ":" << __LINE__ << ": " << left << std::setw(40) << #s << "  -> [ "; s; cout << "]\n";



MyUniquePtr::MyUniquePtr() : memory{nullptr}, x{0} {
    cout << "Default constructor, memory is 0; ";
}

MyUniquePtr::MyUniquePtr(int x, int y) : memory{new int*[x]}, x{x} {
    cout << "Acquiring memory " << (void*) memory << "; ";
    for(auto i = 0; i < x; ++i) {
        memory[i] = new int[y];
    }
}

MyUniquePtr::MyUniquePtr(MyUniquePtr&& other) : memory{other.memory}, x{other.x} {
    cout << "Move Constructor, passed ownage of " << (void*)memory << "; ";
    other.memory = nullptr;
    other.x = 0;
}

MyUniquePtr::~MyUniquePtr() {
    freeMemory();
}

MyUniquePtr& MyUniquePtr::operator=(MyUniquePtr&& rhs) {
    freeMemory();
    memory = rhs.memory;
    x = rhs.x;
    rhs.memory = nullptr;
    rhs.x = 0;
    cout << "Move Assignment, passed ownage of " << (void*)memory << "; ";
    return *this;
}

int** MyUniquePtr::get() const {
    return memory;
}

void MyUniquePtr::reset() {
    freeMemory();
    memory = nullptr;
    x = 0;
}

void MyUniquePtr::freeMemory() {
    cout << "Releasing memory " << (void*) memory << "; ";
    for(auto i = 0; i < x; ++i) {
        delete[] memory[i];
    }
    delete[] memory;
}

int main() {
    TRACE(MyUniquePtr msp1(5, 5));
    TRACE(msp1.reset());
    TRACE(msp1.reset());
    TRACE(MyUniquePtr msp2(8, 8));

    TRACE(std::vector<MyUniquePtr> vec);
    TRACE(vec.push_back(move(msp2)));
    TRACE(vec.clear());

    TRACE(MyUniquePtr p1(16, 16));
    {
        TRACE(MyUniquePtr p2(move(p1)));
        TRACE(p1 = move(p2));
        cout << "End of sub-scope: ";
    } //p2 is destructed due to end of scope
    cout << "\n";
    TRACE(p1.reset());
    std::cout << "The out of scope destructors are called after:\n";
}
