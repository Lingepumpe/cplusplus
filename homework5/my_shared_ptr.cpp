#include <algorithm> //for std::swap
#include <iostream>
#include <iomanip> //for std::setw
#include <vector>


#include "my_shared_ptr.h"

using namespace std;

#define TRACE(s) cout << __FILE__ ":" << __LINE__ << ": " << left << std::setw(40) << #s << "  -> [ "; s; cout << "]\n";



MySharedPtr::MySharedPtr() : memory{nullptr}, x{0}, reference_counter{new atomic_int{1}} {
    cout << "Default constructor, memory is 0; ";
}

MySharedPtr::MySharedPtr(int x, int y) : memory{new int*[x]}, x{x}, reference_counter{new atomic_int{1}} {
    cout << "Acquiring memory " << (void*) memory << "; ";
    for(auto i = 0; i < x; ++i) {
        memory[i] = new int[y];
    }
}

MySharedPtr::MySharedPtr(const MySharedPtr& other) : memory{other.memory}, x{other.x}, reference_counter{other.reference_counter} {
    int ref_counter_value = reference_counter->fetch_add(1) + 1; //use atomic add+fetch
    cout << (void*) memory << " now referenced by " << ref_counter_value << "; ";
}

MySharedPtr::MySharedPtr(MySharedPtr&& other) {
    swap(*this, other);
}

MySharedPtr::~MySharedPtr() {
    int ref_counter_value = reference_counter->fetch_sub(1) - 1; //use atomic add+fetch
    cout << (void*) memory << " now referenced by " << ref_counter_value << "; ";
    if(ref_counter_value == 0) {
        cout << "Releasing memory " << (void*) memory << "; ";
        delete reference_counter;
        for(auto i = 0; i < x; ++i) {
            delete[] memory[i];
        }
        delete[] memory;
    }
}

MySharedPtr& MySharedPtr::operator=(MySharedPtr rhs) {
    swap(*this, rhs);
    return *this;
}

void swap(MySharedPtr& first, MySharedPtr& second) {
    // enable ADL (not necessary in our case, but good practice)
    using std::swap;

    swap(first.memory, second.memory);
    swap(first.x, second.x);
    swap(first.reference_counter, second.reference_counter);
}

void MySharedPtr::reset() {
    *this = MySharedPtr();
}

int** MySharedPtr::get() const {
    return memory;
}

int MySharedPtr::use_count() const {
    return *reference_counter;
}

int main() {
    TRACE(MySharedPtr msp1(5, 5));
    TRACE(MySharedPtr msp2(msp1));
    TRACE(msp1.reset());
    TRACE(msp1.reset());
    TRACE(MySharedPtr msp3);
    TRACE(msp3 = msp2);
    TRACE(std::vector<MySharedPtr> vec);
    TRACE(vec.push_back(msp3));
    TRACE(vec.push_back(msp3));
    TRACE(vec.clear());
    TRACE(msp3.reset());
    TRACE(msp2.reset());
    std::cout << "The out of scope destructors are called after:\n";
}
