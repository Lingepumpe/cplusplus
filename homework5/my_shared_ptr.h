#ifndef MY_SHARED_PTR_H
#define MY_SHARED_PTR_H

#include <atomic>

class MySharedPtr {
public:
    MySharedPtr();
    MySharedPtr(int x, int y);
    MySharedPtr(const MySharedPtr& other);
    MySharedPtr(MySharedPtr&& other);
    ~MySharedPtr();

    MySharedPtr& operator=(MySharedPtr rhs);

    friend void swap(MySharedPtr& a, MySharedPtr& b);

    void reset();

    int** get() const;
    int use_count() const;

private:
    int** memory;
    int x;
    std::atomic_int* reference_counter;
};

#endif //MY_SHARED_PTR_H
