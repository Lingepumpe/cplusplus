#!/bin/bash
set -e
set -x

CPP="g++"

CPPFLAGS="-std=c++11 -Wall -ggdb"

${CPP} ${CPPFLAGS} main.cpp graph.cpp -o my_graph
${CPP} ${CPPFLAGS} my_shared_ptr.cpp -o my_shared_ptr
${CPP} ${CPPFLAGS} my_unique_ptr.cpp -o my_unique_ptr

