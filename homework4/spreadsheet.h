#ifndef SPREADSHEET_H
#define SPREADSHEET_H

class SpreadsheetCell {
public:
    double value;
};

class Spreadsheet {
public:
    Spreadsheet(int inWidth, int inHeight);
    Spreadsheet(const Spreadsheet& src);
    Spreadsheet& operator=(const Spreadsheet& rhs);
    Spreadsheet(Spreadsheet&& src) noexcept;
    Spreadsheet& operator=(Spreadsheet&& rhs) noexcept;
private:
    void allocMem();
    void freeMem();
    SpreadsheetCell** copyMem() const;
    
    int mWidth, mHeight;
    SpreadsheetCell** mCells;
};

#endif //SPREADSHEET_H
