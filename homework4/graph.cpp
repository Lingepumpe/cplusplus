#include <sstream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <stdexcept>

#include "graph.h"

using namespace std;

Graph::Graph() : V{0} {
}

Graph::Graph(int V) : V{V}, adj(V) {
  for(int i = 0; i < V - 1; ++i) {
    addEdge(rand() % V, rand() % V);
  }
}

Graph::Graph(int V, double ugly) : V{V}, adj(V) { //dummy parameter to chose the constructor that generates connected.
  for(int i = 0; i < V; ++i) {
    addEdge(i, (i+1)%V);
  }  
}

Graph::Graph(const Graph& src) : V{src.V}, E{src.E}, adj{src.adj} {
    std::cout << "Copy constructor invoked\n";
}

Graph& Graph::operator=(Graph other) {
    swap(*this, other);
    return *this;
}

Graph::Graph(Graph&& other) : Graph() {
    std::cout << "Move constructor invoked\n";
    swap(*this, other);
}

/* following the Copy+Swap Idiom we do not need this
Graph& Graph::operator=(Graph&& src) {
    if(this == &src) {
        return *this;
    }
    std::cout << "Move assignment invoked\n";
    V   = src.V;
    E   = src.E;
    adj = std::move(src.adj);
    return *this;
}
*/

void swap(Graph& first, Graph& second) {
    // enable ADL (not necessary in our case, but good practice)
    using std::swap;

    // by swapping the members of two objects,
    // the two objects are effectively swapped
    swap(first.V, second.V);
    swap(first.E, second.E);
    swap(first.adj, second.adj);
}

Graph& Graph::addVertex() {
  V += 1;
  adj.resize(V);
  return *this;
}

Graph& Graph::addEdge(int v, int w) {
  adj[v].push_back(w);
  adj[w].push_back(v);
  ++E;
  return *this;
}

void Graph::search(int v, int w) const {
  if(!isConnected()) {
    throw domain_error{"Graph must be connected"};
  }
  std::cout << "Path from " << v << " to " << w << "\n";
  int cur = v;
  while(cur != w) {
    auto new_cur = adj[cur][rand() % adj[cur].size()];
    std::cout << cur << " -> " << new_cur << "\n";
    cur = new_cur;
  }
}

const std::vector<int>& Graph::getAdj(int v) const {
  return adj[v];
}

bool Graph::isConnected() const {
  if(V == 0) {
    return true;
  }
  vector<bool> isConnectedToZero(V, false);
  vector<bool> visited(V, false);
  isConnectedToZero[0] = true;
  bool haveChange;
  do {
    haveChange = false;
    for(int i = 0; i < V; ++i) {
      if(isConnectedToZero[i] && !visited[i]) {
          visited[i] = true;
          for(int j : adj[i]) {
              isConnectedToZero[j] = true;
              haveChange = true;
          }
      }
    }
  } while(haveChange);
  for(int i = 0; i < V; ++i) {
    if(!isConnectedToZero[i]) {
      return false;
    }
  }
  return true;
}

int Graph::getV() const {
  return V;
}

int Graph::getE() const {
  return E;
}

std::string Graph::toString() {
    return toStringHelper(false);
}

std::string Graph::toString() const  {
    return toStringHelper(true);
}

std::string Graph::toStringHelper(bool isConst) const {
  std::stringstream ss;
  ss << (isConnected() ? "Connected " : "") << (isConst ? "Const " : "Maleable ") << "Graph with V=" << V << ", E=" << E << "\n=====\n";
  for(int v = 0; v < V; ++v) {
    for(auto w : getAdj(v)) {
      ss << v << " is connected to " << w << "\n";
    }
  }
  return ss.str();
}
