#include <vector>
#include <iostream>
#include <string.h>
#include <iomanip>

#include "spreadsheet.h"

#define TRACE(s) std::cout << __FILE__ ":" << __LINE__ << ": " << std::setw(40) << #s << "  -> [ "; s; std::cout << "]\n";

Spreadsheet::Spreadsheet(int inWidth, int inHeight) : mWidth(inWidth), mHeight(inHeight) {
    std::cout << "Calling Spreadsheet(int, int); ";
    allocMem();
}

Spreadsheet::Spreadsheet(const Spreadsheet& src) : mWidth(src.mWidth), mHeight(src.mHeight), mCells(src.copyMem()) {
    std::cout << "Calling Copy Constructor Spreadsheet(const Spreadsheet&); ";
}

Spreadsheet& Spreadsheet::operator=(const Spreadsheet& rhs) {
    std::cout << "Calling Copy Assignment operator=(const Spreadsheet&); ";
    if(this != &rhs) {
        mWidth  = rhs.mWidth;
        mHeight = rhs.mHeight;
        freeMem();
        mCells  = rhs.copyMem();
    }
    return *this;
}

Spreadsheet::Spreadsheet(Spreadsheet&& src) noexcept {
    std::cout << "Calling Move Constructor Spreadsheet(Spreadsheet&&); ";
    std::swap(mWidth, src.mWidth);
    std::swap(mHeight, src.mHeight);
    std::swap(mCells, src.mCells);
}

Spreadsheet& Spreadsheet::operator=(Spreadsheet&& rhs) noexcept {
    std::cout << "Calling Move Assignment operator=(Spreadsheet&&); ";
    if(this != &rhs) {
        mWidth  = rhs.mWidth;
        mHeight = rhs.mHeight;
        freeMem();
        mCells  = rhs.mCells;
    }
    return *this;
}

void Spreadsheet::allocMem() {
    mCells = new SpreadsheetCell*[mWidth];
    for(auto i = 0; i < mWidth; ++i) {
        mCells[i] = new SpreadsheetCell[mHeight];
    }
    
}

void Spreadsheet::freeMem() {
    for(auto i = 0; i < mWidth; ++i) {
        delete[] mCells[i];
    }
    delete[] mCells;
}

SpreadsheetCell** Spreadsheet::copyMem() const {
    SpreadsheetCell** result = new SpreadsheetCell*[mWidth];
    for(auto i = 0; i < mWidth; ++i) {
        result[i] = new SpreadsheetCell[mHeight];
        memcpy(result[i], mCells[i], mHeight*sizeof(SpreadsheetCell));
    }
    return result;
}


Spreadsheet CreateObject() {
    return Spreadsheet(3, 2);
}

int main() {
    std::vector<Spreadsheet> vec;
    for(auto i = 0; i < 2; ++i) {
        TRACE(vec.push_back(Spreadsheet(100, 100))); //calls Spreadsheet(int, int) + Spreadsheet(Spreadsheet&&) + whatever push_back does
    }
    TRACE(Spreadsheet s(2, 3)); //calls Spreadsheet(int, int), 1 time (3 times total now)
    TRACE(s = CreateObject()); //calls Spreadsheet(int, int) and then operator=(Spreadsheet&&)
    TRACE(Spreadsheet s2(5, 6)); //Spreadsheet(int, int)
    TRACE(s2 = s); //this actually copies, calling operator=(const Spreadsheet&)
    TRACE(Spreadsheet s3 = s); //this calls the copy constructor Spreadsheet(const Spreadsheet&)

    //final count: 5*Spreadsheet(int,int), 2*Move Constructor, 1 Move Assignment, 1*Copy Assignment, 1*Copy Constructor
    //also whatever push_back does additionally, on my system 1* move constructor (presumably due to the vector doing a resize)
}
