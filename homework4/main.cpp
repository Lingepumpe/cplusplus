#include <iostream>
#include <vector>

#include "graph.h"

#define PRINT_GRAPH(s) { std::cout << #s ": -> " << s.toString() << std::endl; }

int main() {
  srand (time(NULL));

  Graph g1{2};
  Graph g2{g1};
  g2.addVertex();
  Graph g3;
  g3 = g2;
  g3.addEdge(2, 1);

  PRINT_GRAPH(g1);
  PRINT_GRAPH(g2);
  PRINT_GRAPH(g3);

  std::cout << "Now lets try some of the move constructor and assignment functions\n";
  Graph g4{std::move(g3)}; //first call of the move constructor
  std::vector<Graph> vec;
  vec.push_back(Graph{4}); //second call of move constructor
  Graph g5;
  g5 = std::move(Graph{1}); //third call of the move constructor
  PRINT_GRAPH(g4);
  PRINT_GRAPH(g5);
}
