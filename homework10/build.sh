#!/bin/bash
set -e
set -x

CPP="g++"

CPPFLAGS="-std=c++11 -Wall -ggdb"

${CPP} ${CPPFLAGS} factorial.cpp -o factorial
${CPP} ${CPPFLAGS} var_arg_ptr_no_ptr.cpp -o var_arg

