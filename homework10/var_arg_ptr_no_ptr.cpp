#include <iostream>
#include <vector>

using namespace std;

template<typename T>
int count_occurences(const T& val, const vector<T>& vec) {
    int result = 0;
    for(auto e : vec) {
        if(e == val) {
            ++result;
        }
    }
    return result;
}

template<typename T>
int count_occurences(const T& val, const vector<T*>& vec) {
    int result = 0;
    for(auto e : vec) {
        if(*e == val) {
            ++result;
        }
    }
    return result;
}



int main() {
    cout << "occurences: " << count_occurences(5, {2, 5, 1, 2, 5, 2, 5}) << "\n";
    int a = 2, b = 5, c = 1;
    cout << "occurences: " << count_occurences(5, {&a, &b, &c}) << "\n";
}
