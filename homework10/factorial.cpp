#include <iostream>
#include <typeindex>
#include <unordered_map>
#include <typeinfo>
#include <string>
#include <sstream>

struct pair_hash {
    template <class T1, class T2>
    std::size_t operator () (const std::pair<T1,T2> &p) const {
        auto h1 = std::hash<T1>{}(p.first);
        auto h2 = std::hash<T2>{}(p.second);

        return h1 ^ h2;  
    }
};

std::unordered_map<std::pair<std::type_index, std::string>, std::string, pair_hash> cached_factorials;
std::unordered_map<long, long> cached_factorials_long;

template<typename T> 
T fact(T t) {
    std::cout << "fact<T>(" << t << ")\n";
    if(t <= 1) {
        return 1;
    }
    if(t >= 10) {
        std::stringstream ss;
        ss << t;
        std::string tstr = ss.str();
        auto it = cached_factorials.find(std::make_pair(std::type_index(typeid(T)), tstr));
        if(it != cached_factorials.end()) {
            std::istringstream iss(it->second);
            T result;
            iss >> result;
            if (!iss || !iss.eof()) {
                std::cout << "Conversion error on " << it->second << "\n";
                return -1;
            }
            std::cout << "Using stored value fact(" << tstr << ") = " << result << "\n";
            return result;
        }
        T result = t * fact(t-1);
        std::stringstream ssres;
        ssres << result;
        cached_factorials[make_pair(std::type_index(typeid(T)), tstr)] = ssres.str();
        std::cout << "Cached storing fact(" << tstr << ") = " << result << "\n";
        return result;
    }
    return t * fact(t-1);
}

template<> 
long fact(long t) {
    std::cout << "fact<long>(" << t << ")\n";
    if(t <= 1) {
        return 1;
    }
    if(t >= 10) {
        auto it = cached_factorials_long.find(t);
        if(it != cached_factorials_long.end()) {
            return it->second;
        }
        long result = t * fact(t-1);
        cached_factorials_long[t] = result;
        return result;
    }
    return t * fact(t-1);
}

int main() {
    std::unordered_map<std::type_index, std::string> type_names;
    std::cout << "fact(10) = " << fact(10) << "\n";
    std::cout << "fact(12) = " << fact(12) << "\n";
    std::cout << "fact(3l) = " << fact(3l) << "\n";
}
