#ifndef DIRECTED_GRAPH_H
#define DIRECTED_GRAPH_H

#include "graph.h"

class DirectedGraph : public Graph {
public:
    DirectedGraph() {}
    DirectedGraph(size_type V) : Graph{V} {}
    virtual ~DirectedGraph() {}

    virtual DirectedGraph& addEdge(size_type v, size_type w) override;
    virtual std::string toString() const override;
};


#endif // DIRECTED_GRAPH_H
