#include <iostream>
#include <sstream>

#include "directed_graph.h"

DirectedGraph& DirectedGraph::addEdge(size_type v, size_type w) {
    adj[v].push_back(w);
    ++E;
    return *this;
}

std::string DirectedGraph::toString() const {
    std::stringstream ss;
    ss << "Directed Graph with V=" << V << ", E=" << E << "\nEdges: ";
    for(size_type v = 0; v < V; ++v) {
        for(auto adjIter = adj[v].begin(); adjIter != adj[v].end(); ++adjIter) {
            size_type w = *adjIter;
            ss << v << "->" << w << ", ";
        }
    }
    ss << "\n";
    return ss.str();


}
