#include "kilometers_estimator.h"

class EfficientKilometerEstimator : public KilometersEstimator {
private:
    constexpr static int kilometersPerLiter = 17;
public:
    virtual int getKilometersLeft() const override {
        return kilometersPerLiter * getLitersLeft();
    }
};
