#include <iostream>

#include "efficient_kilometer_estimator.h"

using namespace std;

int main() {
    KilometersEstimator* ke = new EfficientKilometerEstimator();
    ke->setLitersLeft(10);
    cout << ke->getKilometersLeft() << "\n";
}
