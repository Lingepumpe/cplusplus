#ifndef KILOMETERS_ESTIMATOR_H
#define KILOMETERS_ESTIMATOR_H

class KilometersEstimator
{
public:
    virtual int getKilometersLeft() const {
        return getKilometersPerLiter() * getLitersLeft();
    }
    virtual void setLitersLeft(int inValue) { mLitersLeft = inValue; }
    virtual int  getLitersLeft() const { return mLitersLeft; }
private:
    int mLitersLeft;
    int getKilometersPerLiter() const { return 20; }
};

#endif //KILOMETERS_ESTIMATOR_H
