#include <iostream>

#include "graph.h"
#include "weighted_graph.h"
#include "directed_graph.h"
#include "directed_weighted_graph.h"

using namespace std;

int main() {
    srand(time(nullptr));
    
    Graph g{4};
    g.addRandomEdges(6);
    cout << g.toString() << "\n";
    cout << g.dijkstraSP(0).toString();

    cout << "\n======================\n\n";
    
    WeightedGraph wg{5};
    wg.addRandomEdges(7);
    cout << wg.toString() << "\n";
    cout << wg.dijkstraSP(3).toString();

    cout << "\n======================\n\n";
    
    DirectedGraph dg{5};
    dg.addRandomEdges(9);
    cout << dg.toString() << "\n";
    cout << dg.dijkstraSP(1).toString();

    cout << "\n======================\n\n";

    DirectedWeightedGraph dwg{6};
    dwg.addRandomEdges(8);
    cout << dwg.toString() << "\n";
    cout << dwg.dijkstraSP(2).toString();
}
