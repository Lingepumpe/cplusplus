#!/bin/bash
set -e
set -x

CPP="g++"

CPPFLAGS="-std=c++11 -Wall -ggdb"

${CPP} ${CPPFLAGS} graph.cpp weighted_graph.cpp directed_graph.cpp directed_weighted_graph.cpp graph_main.cpp -o graph
${CPP} ${CPPFLAGS} efficient_kilometer_estimator.cpp -o efficient_kilometer_estimator
