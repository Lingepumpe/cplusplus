#include <sstream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <stdexcept>
#include <limits>
#include <assert.h>

#include "graph.h"
#include "index_min_pq.h"

using namespace std;

Graph::Graph() : V{0} {
}

Graph::Graph(size_type V) : V{V}, adj{V} {
}

Graph::Graph(const Graph& src) : V{src.V}, E{src.E}, adj{src.adj} {
}

Graph::Graph(Graph&& other) : Graph() {
    swap(*this, other);
}

Graph& Graph::operator=(Graph other) {
    swap(*this, other);
    return *this;
}

void swap(Graph& first, Graph& second) {
    // enable ADL (not necessary in our case, but good practice)
    using std::swap;

    // by swapping the members of two objects,
    // the two objects are effectively swapped
    swap(first.V, second.V);
    swap(first.E, second.E);
    swap(first.adj, second.adj);
}

Graph& Graph::addVertex() {
    V += 1;
    adj.resize(V);
    return *this;
}

Graph& Graph::addEdge(size_type v, size_type w) {
    adj[v].push_back(w);
    adj[w].push_back(v);
    ++E;
    return *this;
}

Graph& Graph::addRandomEdges(size_type n) {
    for(size_type i = 0; i < n; ++i) {
        addEdge(rand() % V, rand() % V);
    }
    return *this;
}

Graph& Graph::makeConnected() {
    for(size_type i = 0; i < V; ++i) {
        addEdge(i, (i+1)%V);
    }
    return *this;
}

const list<Graph::size_type>& Graph::getAdj(size_type v) const {
    return adj[v];
}

int Graph::minPath(size_type v, size_type w) const {
    ShortestPathInfo spi = dijkstraSP(v);
    if(spi.distTo[w] == std::numeric_limits<double>::infinity()) {
        return -1;
    } else {
        return (int)spi.distTo[w];
    }
}

Graph::ShortestPathInfo Graph::dijkstraSP(int source) const {
    ShortestPathInfo result(V, source);
    MinIndexedPQ pq{(int)V};
    result.distTo[source] = 0;
    result.from[source] = source;
    pq.insert(source, 0);
    while(!pq.isEmpty()) {
        size_type v = pq.deleteMin();
        //now we relax the vertex "cur"
        for(auto w : adj[v]) {
            assert(result.distTo[v] != -1);
            if(result.distTo[w] > result.distTo[v] + getWeight(v, w) || result.distTo[w] == -1) { //found a better result for distTo[w]
                result.distTo[w] = result.distTo[v] + getWeight(v, w);
                result.from[w] = v;
                if(pq.contains(w)) {
                    pq.changeKey(w, result.distTo[w]);
                } else {
                    pq.insert(w, result.distTo[w]);
                }
            }
        }
    }
    return result;
}

std::string Graph::toString() const  {
    std::stringstream ss;
    ss << "Graph with V=" << V << ", E=" << E << "\nEdges: ";
    for(size_type v = 0; v < V; ++v) {
        for(auto adjIter = adj[v].begin(); adjIter != adj[v].end(); ++adjIter) {
            size_type w = *adjIter;
            if(w > v) { //prevent double printing edges
                ss << v << "<->" << w << ", ";
            } else if(w == v) { //loop
                ss << v << "<->" << v << ", ";
                ++adjIter; //dont print double for loops either
            }
        }
    }
    ss << "\n";
    return ss.str();
}

/********* protected *******/

int Graph::getWeight(size_type v, size_type w) const {
    for(auto possible_w : adj[v]) {
        if(possible_w == w) {
            return 1;
        }
    }
    return -1;
}

/****** Shortest Path Info ****/

string Graph::ShortestPathInfo::toString() const {
    assert(distTo.size() == from.size());
    std::stringstream res;
    for(size_type i = 0; i < distTo.size(); ++i) {
        if(distTo[i] == -1) {
            res << source << " ~*~> " << i << " not possible\n";
        } else {
            if(i != source) {
                res << source << " ~*~> " << i << ": ";
                string allSteps = to_string(i);
                for(size_type cur = i; from[cur] != source; cur = from[cur]) {
                    allSteps = to_string(from[cur]) + "->" + allSteps;
                }
                res << source << "->" << allSteps << " (dist=" << distTo[i] << ")\n";
            }
        }
    }
    return res.str();
}
