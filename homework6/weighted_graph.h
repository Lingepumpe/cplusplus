#ifndef WEIGHTED_GRAPH_H
#define WEIGHTED_GRAPH_H

#include "graph.h"
#include "assert.h"

using namespace std;

class WeightedGraph : public Graph {
public:
    WeightedGraph() = default;
    WeightedGraph(size_type V);
    virtual ~WeightedGraph() {}

    virtual WeightedGraph& addVertex() override;
    virtual WeightedGraph& addEdge(size_type v, size_type w) override;
    virtual WeightedGraph& addEdge(size_type v, size_type w, unsigned int weight);
    virtual int getWeight(size_type v, size_type w) const override; //returns the smallest weight of an edge between v and w

    virtual WeightedGraph& addRandomEdges(size_type n) override; //adds n random edges with weights between 0 and 10

    virtual std::string toString() const override;

protected:
    vector<list<unsigned int>> weight;
};


#endif // WEIGHTED_GRAPH_H
