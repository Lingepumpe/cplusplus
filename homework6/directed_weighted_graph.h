#ifndef DIRECTED_WEIGHTED_GRAPH_H
#define DIRECTED_WEIGHTED_GRAPH_H

#include "graph.h"
#include "directed_graph.h"
#include "weighted_graph.h"

class DirectedWeightedGraph : public WeightedGraph {
public:
    DirectedWeightedGraph() {}
    DirectedWeightedGraph(size_type V) : WeightedGraph{V} {}
    virtual ~DirectedWeightedGraph() {}

    virtual DirectedWeightedGraph& addEdge(size_type v, size_type w) override;
    virtual DirectedWeightedGraph& addEdge(size_type v, size_type w, unsigned int weight) override;
    virtual std::string toString() const override;
};

#endif // DIRECTED_WEIGHTED_GRAPH_H
