#include <sstream>

#include "directed_weighted_graph.h"

DirectedWeightedGraph& DirectedWeightedGraph::addEdge(size_type v, size_type w) {
    return addEdge(v, w, 1);
}

DirectedWeightedGraph& DirectedWeightedGraph::addEdge(size_type v, size_type w, unsigned int edgeWeight) {
    adj[v].push_back(w);
    ++E;
    weight[v].push_back(edgeWeight);
    return *this;
}

std::string DirectedWeightedGraph::toString() const {
    std::stringstream ss;
    ss << "Directed Weighted Graph with V=" << V << ", E=" << E << "\nEdges: ";
    for(size_type v = 0; v < V; ++v) {
        auto weightIter = weight[v].begin();
        for(auto adjIter = adj[v].begin(); adjIter != adj[v].end(); ++adjIter, ++weightIter) {
            size_type w = *adjIter;
            ss << v << "->" << w << " (weight=" << *weightIter << "), ";
        }
    }
    ss << "\n";
    return ss.str();
}
