#ifndef MY_LIST_H
#define MY_LIST_H

#include <memory>

template<class T>
class MyList {
public:
    MyList(); //construct empty graph

    MyList(const MyList& src); //copy constructor
    MyList(MyList&& src); //move constructor

    virtual ~MyList() {} //destructor

    MyList<T>& operator=(MyList<T> other); //copy assign constructor

    template<class U>
    friend void swap(MyList<U>& first, MyList<U>& second); //nothrow

    void push_back(const T& val);
    void push_back(T&& val);
    void append_list(const MyList<T>& other);
    void printAll() const;
    void printAllReverse() const;

private:
    template<class U>
    struct Elem {
        Elem() = delete;
        Elem(const U& data, Elem<U>* prev) : data{data}, prev{prev} {}
        Elem(U&& data, Elem<U>* prev) : data(data), prev(prev) {}
        U data;
        std::unique_ptr<Elem<U>> next;
        Elem<U>*                 prev;
    };

    void push_back_helper(std::unique_ptr<Elem<T>>&& e);
    
    std::unique_ptr<Elem<T>> m_head;
    Elem<T>* m_tail;
    //template<class U> static U foo; //template variables require c++14, no other changes (except remove comment at start) required
};

#endif //MY_LIST_H
