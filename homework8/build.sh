#!/bin/bash
set -e
set -x

CPP="g++"

CPPFLAGS="-std=c++11 -Wall -ggdb"

${CPP} ${CPPFLAGS} my_finds.cpp -o finds
${CPP} ${CPPFLAGS} my_list.cpp -o list

