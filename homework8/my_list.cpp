#include <utility>
#include <iostream>
#include <assert.h>

#include "my_list.h"

using namespace std;

template<class T>
MyList<T>::MyList() : m_tail(nullptr) {
}

template<class T>
MyList<T>::MyList(const MyList<T>& src) : MyList()  {
    append_list(src);
}

template<class T>
MyList<T>::MyList(MyList<T>&& other) : MyList() {
    swap(*this, other);
}

template<class T>
MyList<T>& MyList<T>::operator=(MyList<T> other) {
    swap(*this, other);
    return *this;
}

template<class T>
void swap(MyList<T>& first, MyList<T>& second) {
    // enable ADL (not necessary in our case, but good practice)
    using std::swap;

    // by swapping the members of two objects,
    // the two objects are effectively swapped
    swap(first.m_head, second.m_head);
    swap(first.m_tail, second.m_tail);
}

template<class T>
void MyList<T>::push_back(const T& val) {
    unique_ptr<Elem<T>> new_elem{new Elem<T>{val, m_tail}};
    push_back_helper(move(new_elem));
}

template<class T>
void MyList<T>::push_back(T&& val) {
    unique_ptr<Elem<T>> new_elem{new Elem<T>{val, m_tail}};
    push_back_helper(move(new_elem));
}

template<class T>
void MyList<T>::append_list(const MyList& other) {
    Elem<T>* our_old_tail = m_tail; //to prevent infinite looping when appending to ourself
    for(Elem<T>* cur = other.m_head.get(); cur != nullptr; cur = cur->next.get()) {
        push_back(cur->data);
        if(cur == our_old_tail) {
            break;
        }
    }
}

template<class T>
void MyList<T>::printAll() const {
    cout << "MyList contains: ";
    for(Elem<T>* cur = m_head.get(); cur != nullptr; cur = cur->next.get()) {
        cout << cur->data << ", ";
    }
    cout << "\n";
}

template<class T>
void MyList<T>::printAllReverse() const {
    cout << "MyList contains (reverse): ";
    for(Elem<T>* cur = m_tail; cur != nullptr; cur = cur->prev) {
        cout << cur->data << ", ";
    }
    cout << "\n";
}


/****************** private ***************/

template<class T>
void MyList<T>::push_back_helper(unique_ptr<Elem<T>>&& new_elem) {
    Elem<T>* new_elem_ptr = new_elem.get();
    if(m_head.get() == nullptr) {
        m_head = move(new_elem);
    } else {
        assert(m_tail != nullptr);
        assert(m_tail->next.get() == nullptr);
        m_tail->next = move(new_elem);
    }
    m_tail = new_elem_ptr;

}

/********** main ********/

int main() {
    MyList<char> ml;
    ml.printAll();
    ml.push_back('h');
    ml.push_back('e');
    ml.push_back('l');
    ml.push_back('l');
    ml.push_back('o');
    ml.push_back(' ');
    ml.push_back('w');
    ml.push_back('o');
    ml.push_back('r');
    ml.push_back('l');
    ml.push_back('d');
    ml.printAll();
    ml.printAllReverse();
    MyList<char> ml2;
    ml2.push_back('!');
    ml.append_list(ml2);
    ml.printAll();
    ml.append_list(ml);
    ml.printAll();
}

