#include <iostream>
#include <vector>
#include <list>

using namespace std;


template<class InputIterator, class UnaryPredicate>
vector<typename iterator_traits<InputIterator>::value_type> find_if (InputIterator first, InputIterator last, UnaryPredicate pred) {
    list<typename iterator_traits<InputIterator>::value_type> results;
    for(; first != last; ++first) {
        if (pred(*first)) {
            results.push_back(*first);
        }
    }
    
    vector<typename iterator_traits <InputIterator>::value_type> converted_result;
    converted_result.reserve(results.size());
    converted_result.insert(converted_result.end(), make_move_iterator(begin(results)), make_move_iterator(end(results)));
    return converted_result;
}


template<class InputIterator, class T>
vector<typename iterator_traits<InputIterator>::value_type> find (InputIterator first, InputIterator last, const T& val)
{
    return find_if(first, last, [&val] (const T& cur) { return cur == val; });
}

int main() {
    vector<int> vec = {5, 1, 2, 3, 13, 123, 123, 21, 321, 52, 6, 7, 7, 34, 1321, 312, 52, 5, 3, 32, 532, 532, 5, 235,2, 1, 12, 32, 3, 12, 21, 312, 312, 31 };
    auto results = find(vec.begin(), vec.end(), 312);
    cout << "results contains: ";
    for(auto elem : results) {
        cout << elem << ", ";
    }
    cout << "\n";

    auto results2 = find_if(vec.begin(), vec.end(), [] (int elem) { return elem > 34; });
    cout << "results2 contains: ";
    for(auto elem : results2) {
        cout << elem << ", ";
    }
    cout << "\n";    
}
